import { log } from './utils/promisse-handlers.js';
import { notaService as service } from './nota/service.js'
import './utils/arrays-helper.js'

document
    .querySelector('#myButton')
    .onclick = () =>
        service.sumItens("2143")
            .then(log)
            .catch(console.log)