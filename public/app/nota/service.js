import { handleStatus } from '../utils/promisse-handlers.js'
import { partialize, pipe } from '../utils/operators.js';

const getItemsFromNotas = notas => notas.$flatMap(nota => nota.itens)
const filterItemsByCode = (code, items) => items.filter(item => item.codigo === code)
const sumItemsValue = items => items.reduce((total, iten) => total + iten.valor, 0)

export const notaService = {

    listAll() {
        return fetch('http://localhost:3000/notas')
            .then(handleStatus)
    },
    sumItens(code) {

        return this.listAll().then(
            pipe(
                getItemsFromNotas,
                partialize(filterItemsByCode, code),
                sumItemsValue
            )
        )
}

}